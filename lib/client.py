#!/usr/bin/python3

import subprocess, os
import paho.mqtt.client as mqtt

from rpi_backlight import Backlight
from dotenv import load_dotenv

load_dotenv()

os.environ['DISPLAY'] = ":0"
backlight = Backlight()

location = os.getenv("LOCATION")
mqttUser = os.getenv("MQTT_USER")
mqttPass = os.getenv("MQTT_PASS")
mqttHost = os.getenv("MQTT_HOST")

topicPrefix = "hq/"
clientId = "dashboard/" + location

displayOnTopic = topicPrefix + clientId + "/display/on"
displayBrightnessTopic = topicPrefix + clientId + "/display/brightness"

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    client.subscribe(displayOnTopic)
    client.subscribe(displayBrightnessTopic)

def on_message(client, userdata, message):
    if message.topic == displayOnTopic:
        subprocess.call('xset dpms force on', shell=True)

    elif message.topic == displayBrightnessTopic:
        parsedPayload = int(message.payload.decode("utf-8"))

        if parsedPayload in range(0, 101):
            backlight.brightness = parsedPayload

client = mqtt.Client(client_id=clientId, clean_session=True)
client.on_connect = on_connect
client.on_message = on_message

client.tls_set()
client.username_pw_set(mqttUser, password=mqttPass)
client.connect(mqttHost, 8883, 60)

client.loop_forever()
