# HA Dashboard

A Raspberry Pi based display for displaying a dashboard from Home Assistant with a touch screen interface.

[[_TOC_]]

## Hardware

There are several pieces of hardware for this project to operate.
Links will be to where I bought the items for this project (So maybe great for convenience but maybe less so for price).
If an item does not have a purchase link then it is likely an item I had laying around from previous projects or I have forgotten where I got it from originally.

### Raspberry Pi 3B+

The raspi is used to drive the touch screen and to provide WiFi/Ethernet connection to be able to access the HA Dashboard. This will be mounted into the back of the touch screen.

### 7" Touch screen module

To provide an interface for the user to interact with the dashboard I use a 7" touch screen connected to the raspi display port. I bought this from [Kjell & Company - 87843](https://www.kjell.com/se/produkter/dator/raspberry-pi/raspberry-pi-touchskarm-7-p87843)


### 7" Touch screen module & Raspberry Pi case

And to finalise the full assembly a case to contain everything also brought from [Kjell & Company - 87869](https://www.kjell.com/se/produkter/dator/raspberry-pi/lada-for-raspberry-pi-och-touchskarm-p87869)


## Software

A small amount of software is needed in order for the display to be able to show the dashboard and to allow Home Assistant to control the screen brightness.

### OS

I chose Rasbian Lite as the OS to run on the raspi with the default pi user removed. This project does not need a desktop environment as we will be booting directly into the chromium instance in kiosk mode.

### Python 3

Python 3 is used to handle communication with the MQTT broker and the system, I chose this as I wanted to try out using Python for a project and this seemed like a good enough fit for it.

### Chromium 

Chromium is used as the browser to display the dashboard in kiosk mode.


## Installation

To install clone this repository into /opt/dasboard

once done run the install.sh script as root and everything will be set up minus trusting the hQ root CA and filling in app secrets.

### Root CA

hQ has a custom root CA that needs to be trusted on the device as all traffic is required to go over https or tls. to install the hQ root cert do:

```
sudo mkdir /usr/share/ca-certificates/hQ
```

copy the root ca into `/usr/share/ca-certificates/hQ`

then:

```
sudo dpkg-reconfigure ca-certificates
```

Side note: if the certificate is in pem format then you can convert to crt by doing:
```
openssl x509 -in foo.pem -inform PEM -out foo.crt
```

once the manual data is entered just restart the device and everything should begin!
