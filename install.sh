#!/bin/bash

# Clone the repository to /opt/dashboard and run this script to set it up for dashboard usage

if [ "$EUID" -ne 0 ]; then
  echo "Please run as root"
  exit
fi

SCRIPT_PATH="$(cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
OPT_PATH="/opt/dashboard"

if [ "$SCRIPT_PATH" != "$OPT_PATH" ]; then
  echo "please run script from inside $OPT_PATH directory"
  exit
fi

# chromium

if [[ "$(command -v chromium-browser)" =~ "chromium-browser" ]]; then
  echo "Chromium is installed"
else
  apt update
  apt install chromium-browser -y
fi

# Python3

if [[ "$(command -v python3)" =~ "python3" ]]; then
  echo "Python3 is installed"
else
  apt update
  apt install python3 -y
fi

# Pip3

if [[ "$(command -v pip3)" =~ "pip3" ]]; then
  echo "Pip3 is installed"
else
  apt update
  apt install python3-pip -y
fi

# Disable Splash screen

SPLASH_DISABLED="$(sed -nr "/^\[all\]/ { :l /^disable_splash[ ]*=/ { s/.*=[ ]*//; p; q;}; n; b l;}" /boot/config.txt)"

if [ "$SPLASH_DISABLED" == "" ]; then
  if grep -Fxq "[all]" /boot/config.txt; then
    sed -i 's/^\[all\]$/\[all\]\ndisable_splash=1/' /boot/config.txt
  else
    echo "[all]" >> /boot/config.txt
    echo "disable_splash=1" >> /boot/config.txt
  fi
else
  sed -i '/^\[all\]$/,/^\[/ s/^disable_splash=0/disable_splash=1/' /boot/config.txt
fi

# Remove text message under splash screen

sudo sed -i '/^\s*message_sprite = Sprite();$/s/^/#/' /usr/share/plymouth/themes/pix/pix.script
sudo sed -i '/^\s*message_sprite.SetPosition(screen_width \* 0.1, screen_height \* 0.9, 10000);$/s/^/#/' /usr/share/plymouth/themes/pix/pix.script
sudo sed -i '/^\s*my_image = Image.Text(text, 1, 1, 1);$/s/^/#/' /usr/share/plymouth/themes/pix/pix.script
sudo sed -i '/^\s*message_sprite.SetImage(my_image);$/s/^/#/' /usr/share/plymouth/themes/pix/pix.script

# move boot messages

sed -i 's/console=tty1/console=tty3/' /boot/cmdline.txt

# Remove other things

sed -zi '/splash/!s/\n$/ splash\n/' /boot/cmdline.txt
sed -zi '/quiet/!s/\n$/ quiet\n/' /boot/cmdline.txt
sed -zi '/plymouth\.ignore-serial-consoles/!s/\n$/ plymouth.ignore-serial-consoles\n/' /boot/cmdline.txt
sed -zi '/logo.nologo/!s/\n$/ logo.nologo\n/' /boot/cmdline.txt
sed -zi '/vt\.global_cursor_default=0/!s/\n$/ vt.global_cursor_default=0\n/' /boot/cmdline.txt

# Replace splash image

cp -uf "$OPT_PATH/splash.png" /usr/share/plymouth/themes/pix/splash.png

echo "set custom boot splash screen"

# create dashboard user

if id "dashboard" > /dev/null 2>&1; then
  echo "dashboard user exists"
else
  echo "Creating dashboard user"
  adduser --disabled-password --gecos "" --quiet dashboard 
fi

# auto login dashboard user

systemctl set-default graphical.target
ln -fs /etc/systemd/system/autologin@.service /etc/systemd/system/getty.target.wants/getty@tty1.service

sed -ie "s/^\(#\|\)autologin-user=.*/autologin-user=dashboard/" /etc/lightdm/lightdm.conf

echo "Set dashboard user to auto login"

# LXDE autostart

mkdir -p /home/dashboard/.config/lxsession/LXDE-pi
cp -uf "$OPT_PATH/lxde-autostart" /home/dashboard/.config/lxsession/LXDE-pi/autostart

echo "copied LXDE autostart file to dashboard user"

# MQTT Client

if [ ! -f "$OPT_PATH/lib/.env" ]; then
  echo "Creating empty .env file for MQTT client"

  echo "LOCATION=" > "$OPT_PATH/lib/.env"
  echo "MQTT_USER=" >> "$OPT_PATH/lib/.env"
  echo "MQTT_PASS=" >> "$OPT_PATH/lib/.env"
  echo "MQTT_HOST=" >> "$OPT_PATH/lib/.env"
fi

su dashboard -c "cd '$OPT_PATH/lib' && pip3 install -r ./requirements.txt"

echo 'SUBSYSTEM=="backlight",RUN+="/bin/chmod 666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power"' | sudo tee -a /etc/udev/rules.d/backlight-permissions.rules

# permission reset

chown dashboard:dashboard -R /home/dashboard
chown dashboard:dashboard -R "$OPT_PATH/lib"

# Systemd 

cp -uf "$OPT_PATH/dashboard.service" /etc/systemd/system/dashboard.service
systemctl daemon-reload
systemctl enable dashboard.service
systemctl restart dashboard.service

# Manual items to do

echo "You need to manually update the .env file with the current environment variables needed for the MQTT client"
echo "You also need to load the root CA into the systems trusted certificates"
echo "reboot the device to complete the installation"
